# wwm.leetcode.helper.issue

#### 介绍
wwm.leetcode.helper 是 C# 刷LeetCode帮助包
本仓库为wwm.leetcode.helper帮助包收集ISSUE仓库

wwm.leetcode.helper 特点：
1. 自动下载原题
2. 自动将原题测试案例生成测试数据
3. 自动添加方法默认返回值
4. 自动比较方法返回值，并输出正确与错误数据的对比
5. 自动在新建的文件中增加测试接口ITest

#### 软件架构
- .NET6
- Playwright
- Roslyn


#### 安装教程

1.  从nuget上搜索wwm.leetcode.helper
2.  拉取题库使用的是Playwright工具, 如果报找不到Playwright或报本地未安装Chromium浏览器,请前往[官网](https://playwright.dev/dotnet/docs/intro#first-project)下载安装
测试安装playwright主要安装步骤,关键是安装缺失的Chromium浏览器
```
# Create project
dotnet new console -n PlaywrightDemo
cd PlaywrightDemo

# Add project dependency
dotnet add package Microsoft.Playwright
# Build the project
dotnet build
# Install required browsers
pwsh bin\Debug\netX\playwright.ps1 install
```

#### 使用说明
方式一:
创建Console控制台应用程序，Program.cs 内容如下
```c#

//使用手动输入题目url的方式生成测试文件
Console.WriteLine($"Hello, {nameof(wwm.LeetCodeHelper)}");
List<string> problemUrls = new List<string>()
{
    //"https://leetcode-cn.com/problems/repeated-string-match/"
};

if (problemUrls.Any())
    await LeetCodeHelper.GetContentFromBrowserAsync(problemUrls, "Solutions", "Content", SlowMo: 1, Headless: true);
else
    TestResultHelper.InvokeAllTest(typeof(Program).Assembly.GetName().Name);

Console.Write("按任意键退出...");
Console.ReadKey();
```

方式二:
```
//使用自动生成当天题目文件且自动测试的方式
await LeetCodeHelper.GetTodayRecordContentOrInvokeTestAsync(typeof(Program).Assembly.GetName().Name, "Solutions", "Content", SlowMo: 0, Headless: false);
Console.Write("按任意键退出...");
Console.ReadKey();
```
具体使用可参照作者本人刷leetcode仓库地址 https://gitee.com/wwmin/LeetCodeCSharp.git
#### 参与贡献

1.  欢迎安装nuget包试用
2.  欢迎积极提issue


#### 使用方法

1.  输入leetcode url地址，运行可自动生成C#原题及测试数据文件，文件自动下载到指定文件夹路径下
2.  使用手动输入题URL的方式时, 测试代码前需要先注释掉 Program.cs 下的problemUrls url值
3.  需要在待测文件类上继承 ITest 接口，不需要测试类要删除 ITest
4.  然后F5运行测试，自动比较结果，输出正确结果或错误结果比较
